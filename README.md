frd-console
=========
frd-console is a console version of [Free Rapid Downloader](http://wordrider.net/freerapid/) but it can be used where no interactive GUI can be showed.

You can download it [here](https://bitbucket.org/stmare/fred-console/downloads)

Installation
---------------
First of all the Free Rapid Downloader must be downloaded and launched. Wait for plugins to be downloaded and installed. See directory ~/.FRD/plugins on Linux, or %HOMEPATH%\AppData\Roaming\VitySoft\FRD\plugins on Windows.

Copy frd-console.jar to suitable location. On Libreellec or Openelec it may be at /storage/frd-console.jar

Start
-------------
```
java -jar frd-console.jar -h
```
It returns help which on Linux under account jan looks like this:
```
usage: java -jar frd-console.jar
 -a <arg>               Add link for download and exit when other instance
                        of frd-console already running
 -d <arg>               Downloads directory. Default is:
                        /home/jan/Downloads
 -h                     This help
 -i <arg>               Input links file. Default is:
                        /home/jan/.FRD/inputlinks.txt
 -ma <arg>              Max download attempts for one file. Default is: 10
 -mc <arg>              Max download connections. Default is: 10
 -n                     Normalize destination filename by stripping
                        diacritics
 -p <arg>               Plugins directory. Default is:
                        /home/jan/.FRD/plugins
 -proxyHost <arg>       Proxy host
 -proxyPassword <arg>   Proxy password
 -proxyPort <arg>       Proxy port
 -proxyUser <arg>       Proxy username
 -r <arg>               Results file. Default is:
                        /home/jan/.FRD/results.txt
 -t                     Use temporary file name when file downloading
Example addink a link:
echo "https://uloz.to/!Lc1LozWEfCK5/astell-kern-ak300-navod-k-pouziti-cz-pdf" >> ~/.FRD/inputlinks.txt
```

Normal Start with Defaults
----------------
```
java -jar frd-console.jar
```
See defaults with locations in help

Installation on a no GUI host
-------------------------------
Example will be for Raspberry PI with Libreelec or Openelec.
Make sure the [java hard FP](http://www.oracle.com/technetwork/java/embedded/embedded-se/downloads/index.html) is installed. Location of installed java may be `/storage/java/linux_armv6_vfp_hflt/jre/bin/java`

Copy content of you local .FRD/plugins - which came by GUI version - to Raspberry to /storage/.FRD/plugins

Start command:
```
/storage/java/linux_armv6_vfp_hflt/jre/bin/java -jar /storage/frd-console.jar -d /var/media/my_hdd/movies -i /var/media/my_hdd/inputlinks.txt
```

To be started automatically after boot, edit `/storage/.config/autostart.sh`
```
/storage/java/linux_armv6_vfp_hflt/jre/bin/java -jar /storage/frd-console.jar -d /var/media/my_hdd/movies -i /var/media/my_hdd/inputlinks.txt &
```

Add links
------------
Each line in inputlinks.txt means one file to be downloaded. Adding link to this file must be quick because program is removing links from this file after download. Maybe best is to use something like this:
```
echo "https://download_location" >> ~/.FRD/inputlinks.txt
```
Maybe it's good idea to add `inputlinks.txt` file to some shared location to be seen by samba share. Don't forget to define `-i input_file_location` properly.

Result
--------
See `results.txt`, `log/log.log` and `Downloads` directory.

Note
------
Only download locations where captcha is missing or captcha is guessed automatically can be entered. Locations like https://uloz.to or https://fastshare.cz work.
