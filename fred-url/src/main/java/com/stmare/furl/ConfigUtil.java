package com.stmare.furl;

import java.io.File;

public class ConfigUtil {
    private final static String CONFIG_DIR = ".fred-url";

    public static File getHomeDir() {
        String homeDir = System.getProperty("user.home");
        File file = new File(homeDir, CONFIG_DIR);
        if (!file.isDirectory()) {
            file.mkdir();
        }
        return file;
    }
}
