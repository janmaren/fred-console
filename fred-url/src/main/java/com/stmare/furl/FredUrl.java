package com.stmare.furl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cz.jmare.upath.UPath;
import cz.jmare.upath.UPathFactory;

import cz.jmare.exception.ExceptionMessage;
import cz.jmare.util.FileLinesUtil;

public class FredUrl extends ApplicationWindow {
    private static final String KEY_SCAN_CLIPBOARD = "scan.clipboard";

	private static final String INPUT_LINES_CHARSET = "UTF-8";

	private static final Logger LOGGER = LoggerFactory.getLogger(FredUrl.class);

	private Image imageInfo;
	private Label statusImagelabel;
	private Label statusTextlabel;

	private CCombo inputLinksCombo;

	private Text addLinkText;
	
	private Clipboard clipboard;

	private Thread monitorClipboardThread;

	private Button scanButton;
	
	FredUrl() {
		super(null);
        LOGGER.info("Opened FredUrl");
	}

    protected Control createContents(Composite parent) {
        getShell().setText("Fred URL");
        
        Composite contentComposite = new Composite (parent, SWT.NONE);
        GridLayout gridLayout = new GridLayout (3, false);
        contentComposite.setLayout (gridLayout);
        
        Label inpLinkslabel = new Label(contentComposite, SWT.NONE);
        inpLinkslabel.setText("Input links path");
        
        inputLinksCombo = new CCombo(contentComposite, SWT.BORDER);
        inputLinksCombo.setToolTipText("Full path to inputlinks.txt, samba supported\n"
                + "Examples:\n"
                + "smb://192.168.0.30/my_hdd/inputlinks.txt\n"
                + "\\\\192.168.0.30\\my_hdd\\inputlinks.txt (on Windows machine)\n"
                + "file:///media/my_hdd/inputlinks.txt\n"
                + "/media/my_hdd/inputlinks.txt");
		GridData inpLinksData = new GridData();
		inpLinksData.horizontalAlignment = GridData.FILL;
		inpLinksData.horizontalSpan = 2;
		inputLinksCombo.setLayoutData(inpLinksData);
    	ConfigStore configStore = ConfigStore.getConfigStore();
    	ArrayList<String> inputLinksHistory = configStore.getInputLinksHistory();
    	updateInputLinksCombo(inputLinksHistory);
		
        Label label = new Label(contentComposite, SWT.NONE);
        label.setText("URL to add");
        
        addLinkText = new Text(contentComposite, SWT.BORDER);
        GridData addLinkGridData = new GridData();
        addLinkGridData.horizontalAlignment = GridData.FILL;
        addLinkGridData.grabExcessHorizontalSpace = true;
        addLinkText.setLayoutData(addLinkGridData);
        addLinkText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
                if (e.keyCode == SWT.CR) {
                	addLink();
                }				
			}
		});
        
	    Button sendButton = new Button(contentComposite, SWT.NONE);
	    sendButton.setText("Add");
		GridData sendButData = new GridData();
		sendButData.widthHint = 101;
		sendButton.setLayoutData(sendButData);
	    sendButton.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                addLink();
            }

			@Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
	    
        Label scanLabel = new Label(contentComposite, SWT.NONE);
        scanLabel.setText("Scan clipboard");
        
        scanButton = new Button(contentComposite, SWT.CHECK);
        scanButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				switchMonitorClipboardThread();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
        boolean scanClipboard = configStore.getBooleanValue(KEY_SCAN_CLIPBOARD, true);
		scanButton.setSelection(scanClipboard);
        
		new Label(contentComposite, SWT.NONE);
		
		Composite toolbarComposite = new Composite(contentComposite, SWT.NONE);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
		data.grabExcessHorizontalSpace = true;
		data.horizontalSpan = 3;
		toolbarComposite.setLayoutData(data);
		toolbarComposite.setLayout(new GridLayout(3, false));
	    
        createStatusbar(toolbarComposite);
        
        clipboard = new Clipboard(getShell().getDisplay());
		if (scanClipboard) {
			switchMonitorClipboardThread();
		}

        
		return contentComposite;
    }

 
    private void switchMonitorClipboardThread() {
    	ConfigStore configStore = ConfigStore.getConfigStore();
    	boolean enabled = scanButton.getSelection();
		configStore.putBooleanValue(KEY_SCAN_CLIPBOARD, enabled);
		if (enabled) {
	        MonitorClipboard monitorClipboard = new MonitorClipboard();
	        monitorClipboardThread = new Thread(monitorClipboard);
	        monitorClipboardThread.start();
		} else {
    		monitorClipboardThread.interrupt();
    		try {
				monitorClipboardThread.join(500);
			} catch (InterruptedException e) {
				// no handling
			}
    		monitorClipboardThread = null;
		}
	}

	private void createStatusbar(Composite toolbarComposite) {
        imageInfo = new Image(toolbarComposite.getDisplay(), FredUrl.class.getResourceAsStream("/image/control.png"));

    	statusImagelabel = new Label(toolbarComposite, SWT.NONE);
    	statusImagelabel.setImage(imageInfo);
    	statusImagelabel.setVisible(false);
    	GridData layoutDataImage = new GridData();
    	layoutDataImage.widthHint = 16;
    	layoutDataImage.heightHint = 16;
		statusImagelabel.setLayoutData(layoutDataImage);

    	statusTextlabel = new Label(toolbarComposite, SWT.NONE);
    	GridData layoutDataLabel = new GridData(SWT.FILL, SWT.FILL, false, false);
    	layoutDataLabel.grabExcessHorizontalSpace = true;
		statusTextlabel.setLayoutData(layoutDataLabel);

        getShell().getDisplay().addFilter(SWT.MouseDown, new Listener() {
            @Override
            public void handleEvent(Event event) {
                disableStatusMessage();
            }});

        getShell().getDisplay().addFilter(SWT.KeyDown, new Listener() {
            @Override
            public void handleEvent(Event event) {
                disableStatusMessage();
            }});
        getShell().getDisplay().addFilter(SWT.HardKeyDown, new Listener() {
            @Override
            public void handleEvent(Event event) {
                disableStatusMessage();
            }});
	}

    public void setWarnStatusMessage(final String message) {
    	Runnable runnable = new Runnable() {
			@Override
			public void run() {
		    	statusImagelabel.setVisible(true);
		    	statusTextlabel.setText(message);
		    	statusTextlabel.setVisible(true);
			}
		};
		getShell().getDisplay().asyncExec(runnable);
    }

    public void setInfoStatusMessage(final String message) {
    	Runnable runnable = new Runnable() {
			@Override
			public void run() {
		    	statusImagelabel.setVisible(true);
		    	statusTextlabel.setText(message);
		    	statusTextlabel.setVisible(true);
			}
		};
		getShell().getDisplay().asyncExec(runnable);
    }
    
    public void disableStatusMessage() {
    	getShell().getDisplay().asyncExec(new Runnable(){
			@Override
			public void run() {
		    	statusImagelabel.setVisible(false);
		    	statusTextlabel.setVisible(false);
			}});
    }

	protected void initializeBounds() {
        final ConfigStore configStore = ConfigStore.getConfigStore();
        int width = configStore.getIntValue("window.width", 600);
        int height = configStore.getIntValue("window.height", 170);
		getShell().setSize(width, height);
		getShell().setMinimumSize(400, 170);
        getShell().addListener(SWT.Resize, new Listener() {
            public void handleEvent(Event e) {
                Rectangle rect = getShell().getClientArea();
                configStore.putIntValue("window.width", rect.width);
                configStore.putIntValue("window.height", rect.height);
            }
        });
	}
	
	class MonitorClipboard implements Runnable {
		String lastData = null;
		
		@Override
		public void run() {
			TextTransfer textTransfer = TextTransfer.getInstance();
			while (!Thread.currentThread().isInterrupted()) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						String data = (String) clipboard.getContents(textTransfer);
						if (data != null && (data.startsWith("http://") || data.startsWith("https://")) && !data.equals(lastData)) {
							addLinkText.setText(data);
							getShell().setFocus();
							lastData = data;
						}
					}
				});
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					return;
				}
			}
		}
		
	}
	
	public void clean() {
		imageInfo.dispose();
		if (monitorClipboardThread != null) {
			monitorClipboardThread.interrupt();
			try {
				monitorClipboardThread.join(1000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	
    private void addLink() {
    	String filepath = inputLinksCombo.getText().trim();
    	if (filepath.equals("")) {
    		setInfoStatusMessage("Input links path is empty");
    		return;
    	}
    	String link = addLinkText.getText().trim();
    	if (link.equals("")) {
    		setInfoStatusMessage("Link to add is empty");
    		return;
    	}
    	try {
			UPath uPath = UPathFactory.getUPath(filepath);
			if (!uPath.isFile()) {
	    		setInfoStatusMessage(filepath + " doesn't exist");
	    		return;
			}
			ArrayList<String> lines;
			try (InputStream inputStream = uPath.createInputStream()){
				lines = FileLinesUtil.readLines(inputStream, INPUT_LINES_CHARSET);
				if (lines.contains(link)) {
		    		setInfoStatusMessage("Not added because already present in file");
		    		updateInputLinksConfig(filepath);
		    		return;
				}
			} catch (IOException e) {
	    		setInfoStatusMessage(ExceptionMessage.getCombinedMessage("Unable to read file", e));
	    		return;			
			}
			lines.add(link);
			try (OutputStream os = uPath.createOutputStream()) {
				FileLinesUtil.writeLines(os, lines, INPUT_LINES_CHARSET);
	    		setInfoStatusMessage("Link added");
	            updateInputLinksConfig(filepath);
			} catch (IOException e) {
				setInfoStatusMessage(ExceptionMessage.getCombinedMessage("Unable to write to file", e));
	    		return;
			}

		} catch (InvalidPathException e) {
			setInfoStatusMessage(ExceptionMessage.getCombinedMessage(filepath + " has bad format", e));
			return;
		}
	}
    
    private void updateInputLinksConfig(String addFilepath) {
    	ConfigStore configStore = ConfigStore.getConfigStore();
    	ArrayList<String> inputLinksHistory = configStore.getInputLinksHistory();
    	if (inputLinksHistory.contains(addFilepath)) {
    		inputLinksHistory.remove(inputLinksHistory.indexOf(addFilepath));
    	}
    	inputLinksHistory.add(0, addFilepath);
    	if (inputLinksHistory.size() > 10) {
    		inputLinksHistory.remove(inputLinksHistory.size() - 1);
    	}
    	configStore.setInputLinksHistory(inputLinksHistory);
    	updateInputLinksCombo(inputLinksHistory);
    }
    
    private void updateInputLinksCombo(ArrayList<String> inputLinksHistory) {
    	inputLinksCombo.setItems(inputLinksHistory.toArray(new String[inputLinksHistory.size()]));
    	inputLinksCombo.select(0);
    }
}
