package com.stmare.furl;

public class FredUrlMain {
    public static void main(String[] args) {
        System.setProperty("APP_HOME", ConfigUtil.getHomeDir().toString());
        FredUrl furl = null;
        furl = new FredUrl();
        furl.setBlockOnOpen(true);
        furl.open();
        furl.clean();
    }
}
