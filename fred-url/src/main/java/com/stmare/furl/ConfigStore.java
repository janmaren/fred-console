package com.stmare.furl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

public class ConfigStore {
    private static final String CONFIG_FILE_NAME = "configuration.properties";
    private static final String CONFIG_INPUT_LINKS = "inputLinks";
    
    private Properties props;
    
    private static ConfigStore configStore;
    
    private ConfigStore() {
        File homeDir = ConfigUtil.getHomeDir();
        File configFile = new File(homeDir, CONFIG_FILE_NAME);
        props = new Properties();
        if (configFile.isFile()) {
            try {
                props.load(new FileInputStream(configFile));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    public void save() {
        File homeDir = ConfigUtil.getHomeDir();
        File configFile = new File(homeDir, CONFIG_FILE_NAME);
        try (FileOutputStream os = new FileOutputStream(configFile)) {
            props.store(os, "Configuration");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    

    public ArrayList<String> getInputLinksHistory() {
        String HostHistoryStr = getValue(CONFIG_INPUT_LINKS, "");
        String[] split = HostHistoryStr.split("\t");
        return new ArrayList<String>(Arrays.asList(split));
    }

    public void setInputLinksHistory(List<String> histList) {
        StringJoiner stringJoiner = new StringJoiner("\t");
        for (String string : histList) {
            stringJoiner.add(string);
        }
        putValue(CONFIG_INPUT_LINKS, stringJoiner.toString());
    }
    
    public String getValue(String key, String defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return val;
    }

    public int getIntValue(String key, int defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return Integer.valueOf(val);
    }

    public void putIntValue(String key, int value) {
        props.put(key, String.valueOf(value));
        save();
    }
    
    public boolean getBooleanValue(String key, boolean defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return Boolean.valueOf(val);
    }

    public void putBooleanValue(String key, boolean value) {
        props.put(key, String.valueOf(value));
        save();
    }
    
    public void putValue(String key, String value) {
        props.put(key, value);
        save();
    }
    
    public static synchronized ConfigStore getConfigStore() {
        if (configStore == null) {
            configStore = new ConfigStore();
        }
        return configStore;
    }
}
