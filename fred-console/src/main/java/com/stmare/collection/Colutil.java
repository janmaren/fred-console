package com.stmare.collection;

import java.util.ArrayList;
import java.util.Collection;

public class Colutil {
    public static <T> Collection<T> filter(Collection<T> collection, IPredicate<T> predicate) {
        return filter(collection, predicate, new ArrayList<T>());
    }
    
    public static <T> Collection<T> filter(Collection<T> collection, IPredicate<T> predicate, Collection<T> resultCollection) {
        for (T element : collection) {
            if (predicate.matches(element)) {
                resultCollection.add(element);
            }
        }
        return resultCollection;
    }    

    public static <T> Collection<T> filterNegative(Collection<T> collection, IPredicate<T> predicate) {
        return filter(collection, predicate, new ArrayList<T>());
    }

    public static <T> Collection<T> filterNegative(Collection<T> collection, IPredicate<T> predicate, Collection<T> resultCollection) {
        for (T element : collection) {
            if (!predicate.matches(element)) {
                resultCollection.add(element);
            }
        }
        return resultCollection;
    }
    
    public static <T> T findFirst(Collection<T> collection, IPredicate<T> predicate) {
        for (T element : collection) {
            if (predicate.matches(element)) {
                return element;
            }
        }
        return null;
    }
    
    public static <T> T findLast(Collection<T> collection, IPredicate<T> predicate) {
        return findLast(collection, predicate, null);
    }
    
    public static <T> T findLast(Collection<T> collection, IPredicate<T> predicate, T defaultValue) {
        T result = null;
        for (T element : collection) {
            if (predicate.matches(element)) {
                result = element;
            }
        }
        return result != null ? result : defaultValue;
    }     
}
