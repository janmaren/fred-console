package com.stmare.collection;

public interface IPredicate<T> {
    boolean matches(T t);
}
