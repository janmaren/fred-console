package cz.stmare.util;

import java.io.File;

import cz.stmare.util.PlatformType;

public class HomeManager {
    private File appDataDirectory;
    
    private String vendorName;
    
    private String appName;
    
    private static HomeManager homeManager;
    
    public HomeManager(String vendorName, String appName) {
        this.vendorName = vendorName;
        this.appName = appName;
        homeManager = this;
    }

    public File getAppDataDirectory() {
        if (appDataDirectory == null) {
            String userHome = null;
            try {
                userHome = System.getProperty("user.home");
            } catch (SecurityException ignore) {
            }
            if (userHome != null) {
                final PlatformType osId = AppHelper.getPlatform();
                if (osId == PlatformType.WINDOWS) {
                    File appDataDir = null;
                    try {
                        String appDataEV = System.getenv("APPDATA");
                        if ((appDataEV != null) && (appDataEV.length() > 0)) {
                            appDataDir = new File(appDataEV);
                        }
                    } catch (SecurityException ignore) {
                    }
                    if ((appDataDir != null) && appDataDir.isDirectory()) {
                        // ${APPDATA}\{vendorId}\${applicationId}
                        String path = vendorName + "\\" + appName + "\\";
                        appDataDirectory = new File(appDataDir, path);
                    } else {
                        // ${userHome}\Application
                        // Data\${vendorId}\${applicationId}
                        String path = "Application Data\\" + vendorName + "\\" + appName + "\\";
                        appDataDirectory = new File(userHome, path);
                    }
                } else if (osId == PlatformType.OS_X) {
                    // ${userHome}/Library/Application Support/${applicationId}
                    String path = "Library/Application Support/" + appName + "/";
                    appDataDirectory = new File(userHome, path);
                } else {
                    // ${userHome}/.${applicationId}/
                    String path = "." + appName + "/";
                    appDataDirectory = new File(userHome, path);
                }
            }
        }
        return appDataDirectory;
    }
    
    public File getDownloadsDirectory() {
        try {
            String userHome = System.getProperty("user.home");
            File userHomeDir = new File(userHome);
            return new File(userHomeDir, "Downloads");
        } catch (SecurityException ignore) {
            
        }
        return null;
    }
    
    public static HomeManager getInstance() {
        if (homeManager == null) {
            throw new IllegalStateException("Home manager not initialized");
        }
        return homeManager;
    }
}
