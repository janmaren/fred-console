package cz.stmare.freerapid.loop;

import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.stmare.freerapid.Config;
import cz.stmare.freerapid.core.tasks.ConsoleDownloadTask;
import cz.stmare.freerapid.model.DownloadByConsoleFile;
import cz.stmare.freerapid.plugimpl.ConsoleStorageSupport;
import cz.stmare.freerapid.plugimpl.ConsoleSupportImpl;
import cz.stmare.freerapid.plugins.webclient.ConsoleDownloadClient;
import cz.vity.freerapid.plugimpl.StandardPluginContextImpl;
import cz.vity.freerapid.plugins.webclient.interfaces.ConfigurationStorageSupport;
import cz.vity.freerapid.plugins.webclient.interfaces.DialogSupport;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpFile;
import cz.vity.freerapid.plugins.webclient.interfaces.MaintainQueueSupport;
import cz.vity.freerapid.plugins.webclient.interfaces.ShareDownloadService;

class DownloadThread extends Thread {
	private final static Logger logger = Logger.getLogger("DownloadThread");
	
    private ShareDownloadService downloadService;
    
    private String downloadUrl;

    private long sleepFirst;
    
    /**
     * Null means all is ok
     */
    private Exception resultException;

    private DownloadEvent downloadEvent;

    private Config config;

    private ConsoleDownloadTask consoleDownloadTask;
    
    public DownloadThread(DownloadEvent downloadEvent, ShareDownloadService downloadService, String downloadUrl, Config config) {
        this(downloadEvent, downloadService, downloadUrl, config, 0);
    }

    public DownloadThread(DownloadEvent downloadEvent, ShareDownloadService downloadService, String downloadUrl, Config config, long sleepFirst) {
        this.downloadEvent = downloadEvent;
        this.downloadService = downloadService;
        this.downloadUrl = downloadUrl;
        this.config = config;
        this.sleepFirst = sleepFirst;
    }

    @Override
    public void run() {
    	logger.log(Level.FINE, "Downloading " + downloadUrl);
        try {
            Thread.sleep(sleepFirst);
        } catch (InterruptedException e) {
        }
        
        URL fileUrl = null;
        try {
            fileUrl = new URL(downloadUrl);
        } catch (MalformedURLException e1) {
            resultException = e1;
            try {
				downloadEvent.ended(this);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Failed downloading " + downloadUrl, e);
			}
            return;
        }
        
        String description = null;

        HttpFile httpFile = new DownloadByConsoleFile(fileUrl, config.getDownloadsDirectory(), description);

        ConsoleDownloadClient httpDownloadClient = new ConsoleDownloadClient();
        if (config.getProxyHost() != null) {
            httpDownloadClient.setProxyType(Proxy.Type.HTTP);
            httpDownloadClient.setProxyURL(config.getProxyHost());
            httpDownloadClient.setProxyPort(config.getProxyPort());
            httpDownloadClient.setProxyUsername(config.getProxyUser());
            httpDownloadClient.setProxyPassword(config.getProxyPassword());
        }
        httpDownloadClient.initClient();
        
        consoleDownloadTask = new ConsoleDownloadTask(httpFile, httpDownloadClient);
        consoleDownloadTask.setUseTemporary(config.getUseTemporary());
        consoleDownloadTask.setNormalizeFileName(config.getNormalizeFileName());
        try {
            DialogSupport dialogSupport = new ConsoleSupportImpl(config.getCaptchaDirectory(), downloadService.getId());
            ConfigurationStorageSupport configurationStorageSupport = new ConsoleStorageSupport();
            MaintainQueueSupport maintainQueueSupport = null;
            downloadService.setPluginContext(StandardPluginContextImpl.create(dialogSupport, configurationStorageSupport, maintainQueueSupport));
            downloadService.run(consoleDownloadTask);
        } catch (Exception e) {
            resultException = e;
        }
        
        downloadEvent.ended(this);
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public Exception getResultException() {
        return resultException;
    }
    
    public boolean isOk() {
        return resultException == null;
    }

    public ShareDownloadService getDownloadService() {
        return downloadService;
    }

    public ConsoleDownloadTask getConsoleDownloadTask() {
        return consoleDownloadTask;
    }
}
