package cz.stmare.freerapid.loop;

import org.java.plugin.registry.PluginDescriptor;

import cz.stmare.freerapid.plugimpl.PluginSearcher.PluginAndDescriptor;
import cz.vity.freerapid.plugins.webclient.interfaces.ShareDownloadService;
import cz.vity.freerapid.utilities.DescriptorUtils;

public class DownloadRequest {
    private String downloadUrl;
    
    private PluginAndDescriptor pluginAndDescriptor;

    private int attempts = 0;
    
    private boolean downloading;
    
    public DownloadRequest(String downloadUrl, PluginAndDescriptor pluginAndDescriptor) {
        super();
        this.downloadUrl = downloadUrl;
        this.pluginAndDescriptor = pluginAndDescriptor;
    }

    public ShareDownloadService getShareDownloadService() {
        return pluginAndDescriptor.shareDownloadService;
    }
    
    public PluginDescriptor getPluginDescriptor() {
        return pluginAndDescriptor.pluginDescriptor;
    }
    
    public int getMaxAllowedDownloads() {
        return DescriptorUtils.getAttribute("maxDownloads", 1, pluginAndDescriptor.pluginDescriptor);
    }

    public boolean isPremium() {
        return DescriptorUtils.getAttribute("premium", false, pluginAndDescriptor.pluginDescriptor);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((downloadUrl == null) ? 0 : downloadUrl.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DownloadRequest other = (DownloadRequest) obj;
        if (downloadUrl == null) {
            if (other.downloadUrl != null)
                return false;
        } else if (!downloadUrl.equals(other.downloadUrl))
            return false;
        return true;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }
}
