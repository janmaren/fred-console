package cz.stmare.freerapid.loop;

public interface LinkByUserEvent {
    void add(String link);
    
    void delete(String link);
}
