package cz.stmare.freerapid.loop;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InputLinksScanner implements LoopEvent, Runnable {
    private static final int DETECT_PERIOD = 3000;

    private List<String> lastLinks = new ArrayList<String>();
    
    private File linksFile;
    
    private LinkByUserEvent linkUserAction;
    
    public InputLinksScanner(File linksFile, LinkByUserEvent linkUserAction) {
        this.linksFile = linksFile;
        this.linkUserAction = linkUserAction;
    }

    @Override
    public void delete(String deleteLink) {
        List<String> currentLinks = getCurrentLinks();
        removeLinkFromArray(deleteLink, currentLinks);
        saveLinks(currentLinks);
        removeLinkFromArray(deleteLink, lastLinks);
    }

    public void removeLinkFromArray(String deleteLink, List<String> links) {
        for (Iterator<String> iterator = links.iterator(); iterator.hasNext();) {
            String link = iterator.next();
            if (link.equals(deleteLink)) {
                iterator.remove();
            }
        }
    }

    private void saveLinks(List<String> currentLinks) {
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(linksFile), "UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException("Error during opening " + linksFile, e);
        }
        try {
            for(int i = 0; i < currentLinks.size(); i++) {  
                bw.write(currentLinks.get(i));  
                bw.newLine();  
            }
        } catch (IOException e) {
            throw new RuntimeException("Error during writing to " + linksFile, e);
        } finally {
            try {
                bw.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void run() {
        long lastModified = 0;
        while (true) {
        	if (linksFile.isFile()) {
	            long timestamp = linksFile.lastModified();
	            if (lastModified != timestamp) {
	                synchronized (this) {
	                    List<String> currentLinks = getCurrentLinks();
	                    for (String lastLink : lastLinks) {
	                        if (!currentLinks.contains(lastLink)) {
	                            linkUserAction.delete(lastLink);
	                        }
	                    }
	                    for (String currentLink : currentLinks) {
	                        if (!lastLinks.contains(currentLink)) {
	                            linkUserAction.add(currentLink);
	                        }
	                    }
	                    lastModified = linksFile.lastModified();
	                    lastLinks = currentLinks;
	                }
	            }
        	}
            try {
                Thread.sleep(DETECT_PERIOD);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private List<String> getCurrentLinks() {
        boolean foundDupl = false;
        List<String> links = new ArrayList<String>();
        if (!linksFile.isFile()) {
            return links;
        }
        BufferedReader in = null;
        try {   
            in = new BufferedReader(new InputStreamReader(new FileInputStream(linksFile), "UTF-8"));
            String str;
            while ((str = in.readLine()) != null) {
                if (links.contains(str)) {
                    foundDupl = true;
                } else {
                    links.add(str);
                }
            }
        } catch (IOException e) {
            if (linksFile.isFile()) {
                throw new RuntimeException("Some problems with file " + linksFile, e);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        if (foundDupl) {
            saveLinks(links);
        }
        return links;
    }
}
