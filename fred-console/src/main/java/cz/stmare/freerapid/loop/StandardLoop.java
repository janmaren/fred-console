package cz.stmare.freerapid.loop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.stmare.freerapid.Config;
import cz.stmare.freerapid.model.DownloadByConsoleFile;
import cz.stmare.freerapid.plugimpl.PluginSearcher;
import cz.stmare.freerapid.plugimpl.PluginSearcher.PluginAndDescriptor;
import cz.vity.freerapid.plugins.exceptions.ErrorDuringDownloadingException;
import cz.vity.freerapid.plugins.exceptions.FailedToLoadCaptchaPictureException;
import cz.vity.freerapid.plugins.exceptions.InvalidURLOrServiceProblemException;
import cz.vity.freerapid.plugins.exceptions.NotEnoughSpaceException;
import cz.vity.freerapid.plugins.exceptions.NotRecoverableDownloadException;
import cz.vity.freerapid.plugins.exceptions.ServiceConnectionProblemException;
import cz.vity.freerapid.plugins.exceptions.YouHaveToWaitException;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpFile;
import cz.vity.freerapid.plugins.webclient.interfaces.ShareDownloadService;


public class StandardLoop implements LinkByUserEvent, DownloadEvent {
	private final static Logger logger = Logger.getLogger("StandardLoop");
	
    private PluginSearcher pluginWrapper;

    private Map<String, DownloadThread> threadsMap = new ConcurrentHashMap<String, DownloadThread>(); 
    
    private List<DownloadRequest> downloadRequests = new ArrayList<DownloadRequest>();
    
    private int currentDownloadsNumber = 0;
    
    private LoopEvent loopEvent;

    private Config config;
    
    public StandardLoop(PluginSearcher pluginWrapper, Config config) {
        this.pluginWrapper = pluginWrapper;
        this.config = config;
    }

    @Override
    public void add(String downloadUrl) {
    	logger.info("Adding: " + downloadUrl);
        PluginAndDescriptor pluginForUrl = pluginWrapper.getPluginForUrl(downloadUrl, config.getPremiumAccountsByService());
        if (pluginForUrl == null) {
            result(downloadUrl, "NoPluginFound");
            cleanup(downloadUrl);
            return;
        }
        DownloadRequest downloadRequest = new DownloadRequest(downloadUrl, pluginForUrl);
        downloadRequests.add(downloadRequest);
        startThreads();
//        ShareDownloadService downloadService = pluginForUrl.shareDownloadService;
//        if (downloadService == null) {
//            result(downloadUrl, "NoPluginFound");
//            cleanup(downloadUrl);
//            return;
//        }
//        DownloadThread downloadThread = new DownloadThread(this, downloadService, downloadUrl, config);
//        downloadThread.start();
//        threadsMap.put(downloadUrl, downloadThread);
    }
    
    private void startThreads() {
        for (DownloadRequest downloadRequest: downloadRequests) {
            if (currentDownloadsNumber >= config.getMaxConnections()) {
                return;
            }

            if (downloadRequest.isDownloading()) {
                continue;
            }
            
            String downloadUrl = downloadRequest.getDownloadUrl();
            ShareDownloadService downloadService = downloadRequest.getShareDownloadService();
            int downloadingServiceCount = countDownloads(downloadService.getId());
            if (downloadingServiceCount < downloadRequest.getMaxAllowedDownloads()) {
                DownloadThread downloadThread = new DownloadThread(this, downloadService, downloadUrl, config);
                downloadThread.start();
                threadsMap.put(downloadUrl, downloadThread);
                downloadRequest.setDownloading(true);
                downloadRequest.setAttempts(1);
                currentDownloadsNumber ++;
            }
        }
    }
    
    private int countDownloads(String serviceId) {
        int count = 0;
        for (DownloadRequest downloadRequest: downloadRequests) {
            if (downloadRequest.getShareDownloadService().getId().equals(serviceId) &&
                    downloadRequest.isDownloading()) {
                count ++;
            }
        }
        return count;
    }

    private void result(String downloadUrl, Exception e) {
        String ex = e.getClass().getSimpleName();
        if (e.getMessage() != null) {
            ex += ":" + e.getMessage();
        }
        result(new String[]{downloadUrl, ex});
    }
    
    private void result(String downloadUrl, String result) {
        result(new String[]{downloadUrl, result});
    }
    
    private void result(String[] res) {
        File resultsFile = config.getResultsFile();
        PrintStream printStream = null;
        try {
            printStream = new PrintStream(new FileOutputStream(resultsFile, true));
            for (int i = 0; i < res.length; i++) {
                if (i > 0) {
                    printStream.append(", ");
                }
                printStream.append('"');
                if (res[i] != null) {
                    printStream.append(res[i]);
                }
                printStream.append('"');
            }
            printStream.append(System.getProperty("line.separator"));
        } catch (FileNotFoundException e) {
        	logger.log(Level.WARNING, "File " + resultsFile + " can not be written", e);
        } finally {
            if (printStream != null) {
                printStream.close();
            }
        }
    }

    @Override
    public void delete(String downloadUrl) {
    	Logger logger = Logger.getLogger("StandardLoop");
    	logger.info("Deleting: " + downloadUrl);
        DownloadThread downloadThread = threadsMap.get(downloadUrl);
        if (downloadThread != null) {
            downloadThread.interrupt();
            threadsMap.remove(downloadUrl);
        }
    }

    @Override
    public void ended(DownloadThread downloadThread) {
        final String downloadUrl = downloadThread.getDownloadUrl();
        HttpFile downloadFile = downloadThread.getConsoleDownloadTask().getDownloadFile();
        if (downloadThread.isOk()) {
            logger.info("Finished successfully: " + downloadThread.getDownloadUrl());
            String dateFrom = null;
            String dateTo = null;
            Float speed = null; // in kb/sec
            if (downloadFile instanceof DownloadByConsoleFile) {
                DownloadByConsoleFile downloadByConsoleFile = (DownloadByConsoleFile) downloadFile;
                SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                dateFrom = timeFormat.format(downloadByConsoleFile.getDownloadStarted());
                dateTo = timeFormat.format(downloadByConsoleFile.getDownloadFinished());
                speed = (float) (downloadByConsoleFile.getDownloaded() / ((downloadByConsoleFile.getDownloadFinished().getTime() - downloadByConsoleFile.getDownloadStarted().getTime()) ));
            }
            result(new String[] {downloadUrl, "OK", downloadFile.getFileName(), String.valueOf(downloadFile.getFileSize()), dateFrom, dateTo, (speed != null ? speed.toString() : null)});
            cleanupSync(downloadUrl);
        } else {
            Exception e = downloadThread.getResultException();
            logger.log(Level.WARNING, "Unable to download " + downloadThread.getDownloadUrl(), e);
            if (!threadsMap.containsKey(downloadUrl)) {
                result(downloadUrl, "Interrupted");
                cleanupSync(downloadUrl);
            } else if (e instanceof ServiceConnectionProblemException) {
                repeatDownloadOrCleanup(downloadThread, downloadUrl);
            } else if (e instanceof NotRecoverableDownloadException) {
                result(downloadUrl, e);
                cleanupSync(downloadUrl);
            } else if (e instanceof NotEnoughSpaceException) {
                result(downloadUrl, e);
                cleanupSync(downloadUrl);
            } else if (e instanceof InvalidURLOrServiceProblemException) {
                result(downloadUrl, e);
                cleanupSync(downloadUrl);
            } else if (e instanceof YouHaveToWaitException) {
                repeatDownloadOrCleanup(downloadThread, downloadUrl);
            } else if (e instanceof FailedToLoadCaptchaPictureException) {
                result(downloadUrl, e);
                cleanupSync(downloadUrl);
            } else if (e instanceof ErrorDuringDownloadingException) {
                result(downloadUrl, e);
                cleanupSync(downloadUrl);
            } else if (e instanceof IOException) {
                if (downloadFile.getDownloaded() > 0) {
                    repeatDownloadOrCleanup(downloadThread, downloadUrl);
                } else {
                    result(downloadUrl, e.getClass().getSimpleName());
                    cleanupSync(downloadUrl);
                }
            } else {
            	logger.log(Level.SEVERE, "Exception not caught", e);
            }
        }
    }

    public void cleanupSync(String downloadUrl) {
        threadsMap.remove(downloadUrl);
        synchronized (loopEvent) {
            loopEvent.delete(downloadUrl);
        }
    }

    public void cleanup(String downloadUrl) {
        threadsMap.remove(downloadUrl);
        loopEvent.delete(downloadUrl);
    }

    public void repeatDownloadOrCleanup(DownloadThread lastDownloadThread, final String downloadUrl) {
    	DownloadRequest downloadRequest = null;
    	for (int i = 0; i < downloadRequests.size(); i++) {
    		DownloadRequest downloadReq = downloadRequests.get(i);
    		if (downloadReq.getDownloadUrl().equals(downloadUrl)) {
    			downloadRequest = downloadReq;
    			break;
    		}
    	}
    	if (downloadRequest == null) {
    		logger.log(Level.SEVERE, "Not found download url " + downloadUrl + " among download requests");
    		return;
    	}
        if (downloadRequest.getAttempts() < downloadRequest.getMaxAllowedDownloads()) { 
            repeatDownload(lastDownloadThread, downloadUrl, downloadRequest);
        } else {
            cleanup(downloadUrl);
        }
    }

    public void repeatDownload(DownloadThread previousDownloadThread, String downloadUrl, DownloadRequest downloadRequest) {
        DownloadThread downloadThreadNew = new DownloadThread(this, previousDownloadThread.getDownloadService(), downloadUrl, config, 2 * 60 * 1000);
        downloadThreadNew.start();
        threadsMap.put(downloadUrl, downloadThreadNew);
        downloadRequest.setAttempts(downloadRequest.getAttempts() + 1);
    }

    public void setLoopEvent(LoopEvent loopEvent) {
        this.loopEvent = loopEvent;
    }
}
