package cz.stmare.freerapid.loop;

public interface DownloadEvent {
    void ended(DownloadThread downloadThread);
}
