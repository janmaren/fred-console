package cz.stmare.freerapid.core.tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.Date;
import java.util.logging.Logger;

import cz.stmare.freerapid.model.DownloadByConsoleFile;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpDownloadClient;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpFile;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpFileDownloadTask;

public class ConsoleDownloadTask implements HttpFileDownloadTask {
    protected HttpDownloadClient client;
    protected HttpFile downloadFile;
    
    private boolean useTemporary = false;
    private boolean normalizeFileName = false;
    
    public ConsoleDownloadTask(HttpFile downloadFile, HttpDownloadClient client) {
        super();
        this.downloadFile = downloadFile;
        this.client = client;
    }

    @Override
    public HttpFile getDownloadFile() {
        return downloadFile;
    }

    @Override
    public HttpDownloadClient getClient() {
        return client;
    }

    @Override
    public void saveToFile(InputStream inputStream) {
        if (!downloadFile.getSaveToDirectory().exists()) {
            if (!downloadFile.getSaveToDirectory().mkdirs()) {
                throw new RuntimeException("Cannot create directory " + downloadFile.getSaveToDirectory());
            }
        }
        
        String destinationFileName = downloadFile.getFileName();
        if (normalizeFileName) {
        	destinationFileName = normalizeString(destinationFileName);
        	if (destinationFileName.trim().equals("")) {
        		destinationFileName = downloadFile.getFileName();
        	}
        }
        String fileName = destinationFileName;
        if (useTemporary) {
            fileName += "." + new Date().getTime() + ".part";
        }
        File resultFile = new File(downloadFile.getSaveToDirectory(), fileName);
        downloadFile.setStoreFile(resultFile);
        
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(resultFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to save file " + resultFile, e);
        }
        int len;
        byte[] buf = new byte[4096];
        long count = 0;
        downloadFile.setDownloaded(count);
        if (downloadFile instanceof DownloadByConsoleFile) {
            ((DownloadByConsoleFile) downloadFile).setDownloadStarted(new Date());
        }
        try {
            while ((len = inputStream.read(buf)) != -1) {
                fileOutputStream.write(buf, 0, len);
                count += len;
                downloadFile.setDownloaded(count);
                if (isTerminated()) {
                    fileOutputStream.flush();
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to download file " + resultFile, e);
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("Unable to close file " + resultFile, e);
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("Unable to close stream", e);
            }
        }
        Logger logger = Logger.getLogger("ConsoleDownloadTask");
        if (useTemporary) {
            File destFile = new File(downloadFile.getSaveToDirectory(), destinationFileName);
            if (!resultFile.renameTo(destFile)) {
                try {
                    destFile.delete();
                    resultFile.renameTo(destFile);
                } catch (Exception e) {
                    // fallback
                    destFile = new File(downloadFile.getSaveToDirectory(), new Date().getTime() + "." + destinationFileName);
                    resultFile.renameTo(destFile);
                }
            }
        }
        
        logger.info("Saved " + downloadFile + " to " + new File(downloadFile.getSaveToDirectory(), downloadFile.getFileName()));
        
        if (downloadFile instanceof DownloadByConsoleFile) {
            ((DownloadByConsoleFile) downloadFile).setDownloadFinished(new Date());
        }
    }

    @Override
    public void sleep(int seconds) throws InterruptedException {
        for (int i = seconds; i > 0; i--) {
            if (isTerminated())
                break;
            Thread.sleep(1000);
        }
        if (isTerminated())
            throw new InterruptedException();
    }

    @Override
    public boolean isTerminated() {
        return Thread.currentThread().isInterrupted();
    }

    public void setUseTemporary(boolean useTemporary) {
        this.useTemporary = useTemporary;
    }

	public void setNormalizeFileName(boolean normalizeFileName) {
		this.normalizeFileName = normalizeFileName;
	}
	
	/**
	 * Strip diacritics
	 * @return
	 */
	private final static String normalizeString(String str) {
		String subjectString = Normalizer.normalize(str, Normalizer.Form.NFD);
		String resultString = subjectString.replaceAll("[^\\x00-\\x7F]", "");
		return resultString;
	}
}
