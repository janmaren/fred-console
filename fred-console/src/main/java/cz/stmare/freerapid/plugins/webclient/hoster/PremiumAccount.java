package cz.stmare.freerapid.plugins.webclient.hoster;

public class PremiumAccount {
    /**
     * Field username
     */
    private String username;
    /**
     * Field password
     */
    private String password;
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
}
