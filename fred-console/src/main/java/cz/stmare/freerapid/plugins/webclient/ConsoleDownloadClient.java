package cz.stmare.freerapid.plugins.webclient;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.logging.Logger;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.NTCredentials;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.URIUtil;

import cz.vity.freerapid.plugins.webclient.DownloadClient;
import cz.vity.freerapid.plugins.webclient.HostConfigurationWithStickyProtocol;
import cz.vity.freerapid.plugins.webclient.ProxySocketFactory;

public class ConsoleDownloadClient extends DownloadClient {
    final static Logger logger = Logger.getLogger(ConsoleDownloadClient.class.getName());

    private int timeout = 120 * 1000;

    private Proxy.Type proxyType;
    
    /**
     * Field proxyURL
     */
    private String proxyURL;
    
    /**
     * Field proxyPort
     */
    private int proxyPort;

    /**
     * Field userName
     */
    private String proxyUsername;

    /**
     * Field password
     */
    private String proxyPassword;
    
    public void initClient() {
        final HttpClientParams clientParams = client.getParams();
        clientParams.setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        clientParams.setParameter(HttpMethodParams.SINGLE_COOKIE_HEADER, true);
        clientParams.setSoTimeout(timeout);
        clientParams.setConnectionManagerTimeout(timeout);
        clientParams.setHttpElementCharset("UTF-8");
        this.client.setHttpConnectionManager(new SimpleHttpConnectionManager(true));
        this.client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
        HttpState initialState = new HttpState();
        
        // proxy
        HostConfiguration configuration = new HostConfiguration();
        if (proxyType != null && (proxyURL == null || proxyPort == 0)) {
            throw new RuntimeException("Set proxy type but not host or port");
        }
        if (proxyType == Proxy.Type.SOCKS) { // Proxy stuff happens here

            configuration = new HostConfigurationWithStickyProtocol();

            Proxy proxy = new Proxy(proxyType, // create custom Socket factory
                    new InetSocketAddress(proxyURL, proxyPort)
            );
            protocol = new Protocol("http", new ProxySocketFactory(proxy), 80);

        } else if (proxyType == Proxy.Type.HTTP) { // we use build in HTTP Proxy support          
            configuration.setProxy(proxyURL, proxyPort);
            if (proxyUsername != null)
                initialState.setProxyCredentials(AuthScope.ANY, new NTCredentials(proxyUsername, proxyPassword, "", ""));
        }
        client.setHostConfiguration(configuration);        
        
        clientParams.setBooleanParameter(HttpClientParams.ALLOW_CIRCULAR_REDIRECTS, true);
        client.setState(initialState);
    }

    @Override
    public GetMethod getGetMethod(final String uri) {
        GetMethod m;
        try {
            m = new GetMethod(uri);
        } catch (IllegalArgumentException e) {
            logger.warning("Invalid URI detected for GetMethod: " + uri + " Trying to reencode ");
            try {
                m = new GetMethod(URIUtil.encodePathQuery(uri));
            } catch (URIException e1) {
                throw e;
            }
        }
        setDefaultsForMethod(m);
        m.setDoAuthentication(hasAuthentification());
        return m;
    }
    
    @Override
    public PostMethod getPostMethod(final String uri) {
        PostMethod m;
        try {
            m = new PostMethod(uri);
        } catch (IllegalArgumentException e) {
            logger.warning("Invalid URI detected for PostMethod: " + uri + " Trying to reencode ");
            try {
                m = new PostMethod(URIUtil.encodePathQuery(uri));
            } catch (URIException e1) {
                throw e;
            }
        }
        setDefaultsForMethod(m);
        m.setDoAuthentication(hasAuthentification());
        return m;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    private boolean hasAuthentification() {
        return proxyType != null && proxyUsername != null;
    }

    public void setProxyType(Proxy.Type proxyType) {
        this.proxyType = proxyType;
    }

    public void setProxyURL(String proxyURL) {
        this.proxyURL = proxyURL;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public void setProxyUsername(String proxyUsername) {
        this.proxyUsername = proxyUsername;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }    
}
