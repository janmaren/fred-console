package cz.stmare.freerapid;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import cz.stmare.freerapid.plugins.webclient.hoster.PremiumAccount;

public class Config {
    private File downloadsDirectory;
    private File inputLinksFile;
    private File resultsFile;
    private File captchaDirectory;
    private File pluginsDirectory;
    private Command command;
    private String addLink;
    private String proxyHost;
    private int proxyPort = -1;
    private String proxyUser;
    private String proxyPassword;
    private int maxAttempts = 10;
    private int maxConnections = 10;
    private Map<String, PremiumAccount> premiumAccountsByService = new HashMap<String, PremiumAccount>();
	private boolean help;
	private boolean useTemporary; // download to temporary file name and after success rename to final name
	private boolean normalizeFileName; // strip diacritics
    
    public File getDownloadsDirectory() {
        return downloadsDirectory;
    }

    public void setDownloadsDirectory(File downloadDirectory) {
        this.downloadsDirectory = downloadDirectory;
    }

    public void setInputLinksFile(File inputLinksFile) {
        this.inputLinksFile = inputLinksFile;
    }

    public File getInputLinksFile() {
        return inputLinksFile;
    }

    public void setResultsFile(File resultsFile) {
        this.resultsFile = resultsFile;
    }

    public File getResultsFile() {
        return resultsFile;
    }

    public void setCaptchaDirectory(File captchaDirectory) {
        this.captchaDirectory = captchaDirectory;
    }

    public File getCaptchaDirectory() {
        return captchaDirectory;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public String getAddLink() {
        return addLink;
    }

    public void setAddLink(String addLink) {
        this.addLink = addLink;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    public void setProxyUser(String proxyUser) {
        this.proxyUser = proxyUser;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public int getMaxAttempts() {
        return maxAttempts;
    }

    public void setMaxAttempts(int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    public File getPluginsDirectory() {
        return pluginsDirectory;
    }

    public void setPluginsDirectory(File pluginsDirectory) {
        this.pluginsDirectory = pluginsDirectory;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public Map<String, PremiumAccount> getPremiumAccountsByService() {
        return premiumAccountsByService;
    }

    public void setPremiumAccountsByService(Map<String, PremiumAccount> premiumAccountsByService) {
        this.premiumAccountsByService = premiumAccountsByService;
    }

	public void setHelp(boolean help) {
		this.help = help;
	}
	
	public boolean isHelp() {
		return this.help;
	}

	public boolean getUseTemporary() {
		return useTemporary;
	}

	public void setUseTemporary(boolean useTemporary) {
		this.useTemporary = useTemporary;
	}

	public boolean getNormalizeFileName() {
		return normalizeFileName;
	}

	public void setNormalizeFileName(boolean normalizeFileName) {
		this.normalizeFileName = normalizeFileName;
	}
}
