package cz.stmare.freerapid;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.stmare.app.Locker;

import cz.stmare.freerapid.loop.InputLinksScanner;
import cz.stmare.freerapid.loop.StandardLoop;
import cz.stmare.freerapid.plugimpl.PluginSearcher;
import cz.stmare.util.HomeManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleMain {
    private static final String INPUTLINKS_NAME = "inputlinks.txt";

    private static final String RESULTS_NAME = "results.txt";

    private static final String CAPTCHA_NAME = "captcha";
    
    private static final String LOCK_NAME = "frd-console.lock";
    
    private static final String PLUGINS_NAME = "plugins";
    
    public static void main(String[] args) {
        HomeManager homeManager = new HomeManager("VitySoft", "FRD");
        
        prepareSLF4j(homeManager);
        Logger logger = Logger.getLogger("ConsoleMain");
    	logger.info("frd-console started");
        
        File defaultLinksFile = new File(homeManager.getAppDataDirectory(), INPUTLINKS_NAME);
        File defaultResultsFile = new File(homeManager.getAppDataDirectory(), RESULTS_NAME);
        File defaultCaptchaDir = new File(homeManager.getAppDataDirectory(), CAPTCHA_NAME);
        File defaultPluginsDir = new File(homeManager.getAppDataDirectory(), PLUGINS_NAME);

        Options options = getOptions(homeManager.getDownloadsDirectory(), defaultLinksFile, defaultResultsFile, defaultCaptchaDir, defaultPluginsDir);
        Config config = getConfig(args, options, homeManager.getDownloadsDirectory(), defaultLinksFile, defaultResultsFile, defaultCaptchaDir, defaultPluginsDir);
        if (config == null) {
            printHelp(options);
            logger.log(Level.SEVERE, "Unable to get config - exit");
            System.exit(-1);
        }
        if (config.isHelp()) {
            printHelp(options);
            logger.info("Showed help and exit");
            System.exit(-1);
        }
        
        logger.info("Input links file: " + config.getInputLinksFile());
        logger.info("Downloads directory: " + config.getDownloadsDirectory());
        logger.info("Results report file: " + config.getResultsFile());
        logger.info("Plugins directory: " + config.getPluginsDirectory());
        
        Boolean addLinkResult = null;
        if (config.getCommand() == Command.ADD) {
            addLinkResult = addLink(config.getAddLink(), config.getInputLinksFile());
        }
        
        File lockFile = new File(homeManager.getAppDataDirectory(), LOCK_NAME);
        Locker locker = new Locker(lockFile);
        boolean tryLock = locker.tryLock();
        if (!tryLock) {
            if (config.getCommand() == Command.ADD) {
                System.exit(0); // added link will be processed by running instance, this can be canceled
            }
            logger.log(Level.WARNING, "Application already running - found lock file " + lockFile);
            System.exit(-1);
        } else {
            if (Boolean.FALSE.equals(addLinkResult)) {
            	logger.log(Level.WARNING, "File not added, exiting");
                locker.unlockFile();
                System.exit(-1);
            }
        }
        
        File downloadsDirectory = config.getDownloadsDirectory();
        if (!downloadsDirectory.isDirectory()) {
            boolean mkdirs = downloadsDirectory.mkdirs();
            if (!mkdirs) {
            	logger.log(Level.SEVERE, "Unable to create directory " + downloadsDirectory);
                System.exit(-1);
            }
        }
        
        File captchaDirectory = config.getCaptchaDirectory();
        if (!captchaDirectory.isDirectory()) {
            boolean mkdirs = captchaDirectory.mkdirs();
            if (!mkdirs) {
            	logger.log(Level.SEVERE, "Unable to create directory " + captchaDirectory);
            }
        }
        
        // All valid, let's continue
        PluginSearcher pluginWrapper = PluginSearcher.getInstance(homeManager.getAppDataDirectory().toString());
        
        if (!config.getInputLinksFile().isFile()) {
        	try {
				config.getInputLinksFile().createNewFile();
			} catch (IOException e) {
				logger.log(Level.WARNING, "Unable to create file " + config.getInputLinksFile());
			}
        }
        
        StandardLoop standardLoop = new StandardLoop(pluginWrapper, config);
        InputLinksScanner inputLinksScanner = new InputLinksScanner(config.getInputLinksFile(), standardLoop);
        standardLoop.setLoopEvent(inputLinksScanner);
        new Thread(inputLinksScanner).start();
        Thread thread = new Thread(inputLinksScanner);
        logger.info("Configuration phase finished - starting");
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            thread.interrupt();
        }
        
        // clean lock
        locker.unlockFile();
    }

	private static void prepareSLF4j(HomeManager homeManager) {
		PrintStream outsys = System.out;
    	NOPPrintStream nop = new NOPPrintStream();
		System.setOut(nop);
    	System.setProperty("DEV_HOME", homeManager.getAppDataDirectory().toString());
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();
		LoggerFactory.getLogger(ConsoleMain.class);
    	System.setOut(outsys);
	}

    private static boolean addLink(String addLink, File file) {
        PrintStream printStream = null;
        try {
            printStream = new PrintStream(file);
            printStream.append(addLink).append(System.getProperty("line.separator"));
        } catch (FileNotFoundException e) {
        	Logger logger = Logger.getLogger("ConsoleMain");
        	logger.log(Level.SEVERE, "Link " + addLink + " can't be added to " + file);
            return false;
        } finally {
            if (printStream != null) {
                printStream.close();
            }
        }
        return true;
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar frd-console.jar", options);
        System.out.println("Example addink a link:\necho \"https://uloz.to/!Lc1LozWEfCK5/astell-kern-ak300-navod-k-pouziti-cz-pdf\" >> ~/.FRD/inputlinks.txt");
    }

    public static Options getOptions(File downloadsDirectory, File defaultLinksFile, File defaultResultsFile, File defaultCaptchaDir, File defaultPluginsDir) {
        Options options = new Options();

        options.addOption("d", true, "Downloads directory. Default is: " + downloadsDirectory);
        options.addOption("i", true, "Input links file. Default is: " + defaultLinksFile);
        options.addOption("r", true, "Results file. Default is: " + defaultResultsFile);
        //options.addOption("c", true, "Captcha directory. Default is: " + defaultCaptchaDir);
        options.addOption("p", true, "Plugins directory. Default is: " + defaultPluginsDir);
        options.addOption("ma", true, "Max download attempts for one file. Default is: 10");
        options.addOption("mc", true, "Max download connections. Default is: 10");
        options.addOption("t", false, "Use temporary file name when file downloading");
        options.addOption("n", false, "Normalize destination filename by stripping diacritics");
        options.addOption("a", true, "Add link for download and exit when other instance of frd-console already running");
        options.addOption("proxyHost", true, "Proxy host");
        options.addOption("proxyPort", true, "Proxy port");
        options.addOption("proxyUser", true, "Proxy username");
        options.addOption("proxyPassword", true, "Proxy password");
        options.addOption("h", false, "This help");
        
        return options;
    }

    public static Config getConfig(String[] args, Options options, File defaultDownloadsDirectory, File defaultLinksFile, File defaultResultsFile, File defaultCaptchaDir, File defaultPluginsDir) {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
    	Logger logger = Logger.getLogger("ConsoleMain");
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
        	logger.log(Level.SEVERE, e.getMessage());
            return null;
        }
        
        Config config = new Config();
        
        String downloadDirectory = cmd.getOptionValue("d");
        config.setDownloadsDirectory(downloadDirectory != null ? new File(downloadDirectory) : defaultDownloadsDirectory);
        
        String inputLinks = cmd.getOptionValue("i");
        config.setInputLinksFile(inputLinks != null ? new File(inputLinks) : defaultLinksFile);
        
        String results = cmd.getOptionValue("r");
        config.setResultsFile(results != null ? new File(results) : defaultResultsFile);

        String captchaDir = cmd.getOptionValue("c");
        config.setCaptchaDirectory(captchaDir != null ? new File(captchaDir) : defaultCaptchaDir);

        String pluginsDir = cmd.getOptionValue("c");
        config.setPluginsDirectory(pluginsDir != null ? new File(pluginsDir) : defaultPluginsDir);

        config.setHelp(cmd.hasOption("h"));
        
        config.setUseTemporary(cmd.hasOption("t"));

        config.setNormalizeFileName(cmd.hasOption("n"));
        
        String maxAttempts = cmd.getOptionValue("ma");
        if (maxAttempts != null) {
            try {
                int maxAttemptsInt = Integer.parseInt(maxAttempts);
                config.setMaxAttempts(maxAttemptsInt);
            } catch (NumberFormatException e) {
            	logger.log(Level.SEVERE, "Max download attempts is not an integer number");
                return null;
            }
        }
        
        String maxConnections = cmd.getOptionValue("mc");
        if (maxConnections != null) {
            try {
                int maxConnectionsInt = Integer.parseInt(maxConnections);
                config.setMaxConnections(maxConnectionsInt);
            } catch (NumberFormatException e) {
            	logger.log(Level.SEVERE, "Max connections is not an integer number");
                return null;
            }
        }        
        
        String proxyHost = cmd.getOptionValue("proxyHost");
        String proxyPort = cmd.getOptionValue("proxyPort");
        int proxyPortInt = -1;
        try {
            if (proxyPort != null) {
                proxyPortInt = Integer.parseInt(proxyPort);
            }
        } catch (Exception e) {
        	logger.log(Level.SEVERE, "Proxy port is not an integer number");
            return null;
        }
        if (proxyHost != null && proxyPortInt != -1) {
        	logger.log(Level.SEVERE, "No proxy port specified");
            return null;
        }
        if (proxyPortInt != -1 && proxyHost == null) {
            proxyHost = "127.0.0.1";
        }
        config.setProxyHost(proxyHost);
        config.setProxyPort(proxyPortInt);
        String proxyUser = cmd.getOptionValue("proxyUser");
        String proxyPassword = cmd.getOptionValue("proxyPassword");
        if (proxyUser != null) {
            config.setProxyUser(proxyUser);
            config.setProxyPassword(proxyPassword);
        }
        
        String commandAdd = cmd.getOptionValue("a");
        if (commandAdd != null) {
            config.setCommand(Command.ADD);
            config.setAddLink(commandAdd);
        }
        return config;
    }
    
    public static class NOPPrintStream extends PrintStream
    {
        public NOPPrintStream() { super(new ByteArrayOutputStream()); }

        public void println(String s) { /* Do nothing */ }
        // You may or may not have to override other methods
    }
}
