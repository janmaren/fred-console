package cz.stmare.freerapid.model;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.vity.freerapid.core.FileTypeIconProvider;
import cz.vity.freerapid.plugins.webclient.DownloadState;
import cz.vity.freerapid.plugins.webclient.FileState;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpFile;

public class DownloadByConsoleFile implements HttpFile {

    private URL fileUrl;
    private File saveToDirectory;
    private String description;
    private Map<String, Object> properties = new HashMap<String, Object>();
    private String fileName;
    
    /**
     * Supposed file size if known
     */
    private long fileSize = -1;

    private DownloadState downloadState;
    
    private FileState fileState;
    private long downloaded;
    
    /**
     * Temporary file for download
     */
    private File storeFile;
    
    /**
     * Time when data started to transfer
     */
    private Date downloadStarted;

    /**
     * Time when transfer finished
     */
    private Date downloadFinished;
    
    public DownloadByConsoleFile(URL fileUrl, File saveToDirectory, String description) {
        this.fileUrl = fileUrl;
        this.saveToDirectory = saveToDirectory;
        this.description = description;
        this.fileName = FileTypeIconProvider.identifyFileName(fileUrl.toString());
    }

    @Override
    public long getFileSize() {
        return fileSize;
    }

    @Override
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public DownloadState getState() {
        return downloadState;
    }

    @Override
    public void setState(DownloadState state) {
        this.downloadState = state;
    }

    @Override
    public FileState getFileState() {
        return fileState;
    }

    @Override
    public void setFileState(FileState state) {
        this.fileState = state;
    }

    @Override
    public URL getFileUrl() {
        return fileUrl;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void setNewURL(URL fileUrl) {
        throw new RuntimeException("Not implemented DownloadByConsoleFile.setNewUrl");
    }

    @Override
    public void setPluginID(String pluginID) {
        throw new RuntimeException("Not implemented DownloadByConsoleFile.setPluginID");
    }

    @Override
    public String getPluginID() {
        throw new RuntimeException("Not implemented DownloadByConsoleFile.getPluginID");
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public long getDownloaded() {
        return downloaded;
    }

    @Override
    public void setDownloaded(long downloaded) {
        this.downloaded = downloaded;
    }

    @Override
    public File getSaveToDirectory() {
        return saveToDirectory;
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public File getStoreFile() {
        return storeFile;
    }

    @Override
    public void setStoreFile(File storeFile) {
        this.storeFile = storeFile;
    }

    @Override
    public long getRealDownload() {
        return downloaded;
    }

    @Override
    public void setResumeSupported(boolean resumeSupported) {
        // not supported
    }

    @Override
    public boolean isResumeSupported() {
        return false;
    }

    public Date getDownloadStarted() {
        return downloadStarted;
    }

    public void setDownloadStarted(Date downloadStarted) {
        this.downloadStarted = downloadStarted;
    }

    public Date getDownloadFinished() {
        return downloadFinished;
    }

    public void setDownloadFinished(Date downloadFinished) {
        this.downloadFinished = downloadFinished;
    }

}
