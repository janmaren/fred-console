package cz.stmare.freerapid.plugimpl;

import java.io.File;
import java.io.FilenameFilter;

public class CaptchaInputScanner implements Runnable {
    private static final int DEFAULT_CHECK_PERIOD = 5000;

    private File directory;
    
    private String filename;
    
    private final static int DEFAULT_TIMEOUT = 1000 * 60 * 5; // 5 minutes
    
    private int timeout = DEFAULT_TIMEOUT;
    
    private String result;

    private boolean timedOut;

    private File imageFile;

    private String filenameStriped;
    
    public CaptchaInputScanner(File imageFile) {
        this(imageFile, DEFAULT_TIMEOUT);
    }

    public CaptchaInputScanner(File imageFile, int timeout) {
        this.imageFile = imageFile;
        this.directory = imageFile.getParentFile();
        this.filename = imageFile.getName();
        this.timeout = timeout;
        int lastIndexOf = this.filename.lastIndexOf(".");
        if (lastIndexOf == -1) {
            throw new RuntimeException("Bad name of image " + imageFile);
        }
        filenameStriped = this.filename.substring(0, lastIndexOf + 1);
    }
    
    @Override
    public void run() {
        long until = System.currentTimeMillis() + timeout;
        while (until > System.currentTimeMillis()) {
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.startsWith(filenameStriped)) {
                        return true;
                    }
                    return false;
                }
            };
            
            File[] files = directory.listFiles(filter);
            if (files == null || files.length == 0) {
                return; // deleted - don't want to guess this captcha
            }
            File foundFirstModifiedFile = foundFirstModifiedFile(files);
            if (foundFirstModifiedFile != null) {
                String foundName = foundFirstModifiedFile.getName();
                if (foundName.startsWith(filename)) {
                    result = foundName.substring(filename.length());
                    if (result.startsWith(".")) {
                        result = result.substring(1);
                    }
                } else {
                    result = foundName.substring(filenameStriped.length()); 
                }
                deleteFiles(files);
                return;
            }
            
            if (Thread.interrupted()) {
                imageFile.delete();
                return;
            }
            
            try {
                Thread.sleep(DEFAULT_CHECK_PERIOD);
            } catch (InterruptedException e) {
                imageFile.delete();
                return;
            }
        }
        timedOut = true;
        imageFile.delete();
    }
    
    private File foundFirstModifiedFile(File[] files) {
        for (File file : files) {
            if (!file.getName().equals(filename)) {
                return file;
            }
        }
        return null;
    }
    
    private static void deleteFiles(File[] files) {
        for (File file : files) {
            file.delete();
        }
    }
    
    /**
     * @return captcha or null when timed out or interrupted or file deleted
     */
    public String getResult() {
        return result;
    }

    public boolean isTimedOut() {
        return timedOut;
    }
}
