package cz.stmare.freerapid.plugimpl;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import cz.vity.freerapid.plugins.webclient.hoster.PremiumAccount;
import cz.vity.freerapid.plugins.webclient.interfaces.DialogSupport;

public class ConsoleSupportImpl implements DialogSupport {
    /**
     * Directory for captchas or for other inputs
     */
    private File directory;
    private String pluginId;
    
    public ConsoleSupportImpl(File directory, String pluginName) {
        this.directory = directory;
        this.pluginId = pluginName;
    }

    @Override
    public PremiumAccount showAccountDialog(PremiumAccount premiumAccount, String dialogTitle) throws Exception {
        return null;
    }

    @Override
    public boolean showOKCancelDialog(Component container, String dialogTitle) throws Exception {
        return false;
    }

    @Override
    public void showOKDialog(Component container, String dialogTitle) throws Exception {
    }

    @Override
    public String askForCaptcha(BufferedImage image) {
        String id = pluginId.replaceAll("\\.", "_");
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date());
        File outputfile = new File(directory, "captcha." + id + "." + timestamp + ".png");
        try {
            ImageIO.write(image, "png", outputfile);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save file " + outputfile, e);
        }
        
        CaptchaInputScanner captchaInputScanner = new CaptchaInputScanner(outputfile);
        Thread captchaInputThread = new Thread(captchaInputScanner);
        captchaInputThread.start();
        try {
            captchaInputThread.join();
            return captchaInputScanner.getResult();
        } catch (InterruptedException e) {
            captchaInputThread.interrupt();
            return null;
        }
    }

    @Override
    public String askForPassword(String name) throws Exception {
        return null;
    }

}
