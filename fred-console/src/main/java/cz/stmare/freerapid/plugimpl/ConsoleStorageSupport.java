package cz.stmare.freerapid.plugimpl;

import cz.vity.freerapid.plugins.webclient.interfaces.ConfigurationStorageSupport;

public class ConsoleStorageSupport implements ConfigurationStorageSupport {

    @Override
    public <E> E loadConfigFromFile(String fileName, Class<E> type) throws Exception {
        return null;
    }

    @Override
    public void storeConfigToFile(Object object, String fileName) throws Exception {
    }

    @Override
    public boolean configFileExists(String fileName) {
        return false;
    }

}
