package cz.stmare.freerapid.plugimpl;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.java.plugin.JpfException;
import org.java.plugin.ObjectFactory;
import org.java.plugin.PluginLifecycleException;
import org.java.plugin.PluginManager;
import org.java.plugin.PluginManager.PluginLocation;
import org.java.plugin.registry.PluginAttribute;
import org.java.plugin.registry.PluginDescriptor;
import org.java.plugin.standard.ShadingPathResolver;
import org.java.plugin.standard.StandardPluginLocation;
import org.java.plugin.util.ExtendedProperties;
import org.java.plugin.util.IoUtil;

import cz.stmare.freerapid.ConsoleMain;
import cz.stmare.freerapid.plugins.webclient.hoster.PremiumAccount;
import cz.vity.freerapid.core.Consts;
import cz.vity.freerapid.plugins.webclient.interfaces.ShareDownloadService;
import cz.vity.freerapid.utilities.DescriptorUtils;
import cz.vity.freerapid.utilities.LogUtils;

public class PluginSearcher {
    final static Logger logger = Logger.getLogger(PluginSearcher.class.getName());
            
    private static PluginSearcher pluginSearcher;
    
    private PluginManager pluginManager;
    
    private List<PluginDescriptor> pluginDescriptors;

    private String appDataDir;

    private PluginSearcher(String appDataDir) {
        super();
        this.appDataDir = appDataDir;
        init();
    }

    public static synchronized PluginSearcher getInstance(String pluginDirectory) {
        if (pluginSearcher == null) {
            pluginSearcher = new PluginSearcher(pluginDirectory);
        }
        return pluginSearcher;
    }
    
    private void init() {
        final ObjectFactory objectFactory = ObjectFactory.newInstance();
        final ShadingPathResolver resolver = new ShadingPathResolver() {
            @Override
            protected URL maybeJarUrl(URL url) throws MalformedURLException {
                /*
                 * This method is overridden to add support for .frp plugins.
                 * Also, the original method uses toLowerCase(Locale.getDefault()).
                 * All classes with these issues: StandardPathResolver, ShadingPathResolver, StandardPluginLocation
                 */
                if ("jar".equalsIgnoreCase(url.getProtocol())) {
                    return url;
                }
                File file = IoUtil.url2file(url);
                if ((file == null) || !file.isFile()) {
                    return url;
                }
                String fileName = file.getName().toLowerCase(Locale.ROOT);
                if (fileName.endsWith(".jar")
                        || fileName.endsWith(".zip")
                        || fileName.endsWith(".frp")) {
                    return new URL("jar:" + IoUtil.file2url(file).toExternalForm() + "!/");
                }
                return url;
            }
        };
        try {
            resolver.configure(new ExtendedProperties());
        } catch (Exception e) {
            LogUtils.processException(logger, e);
        }
        
        pluginManager = objectFactory.createManager(objectFactory.createRegistry(), resolver);
        File[] searchExistingPlugins = searchExistingPlugins();
        ArrayList<PluginLocation> locList = new ArrayList<PluginLocation>();
        for (File pluginFile : searchExistingPlugins) {
            try {
                URL pluginUrl = pluginFile.toURI().toURL();
                final URL context = new URL("jar:" + pluginUrl + "!/");
                final URL manifest = new URL("jar:" + pluginUrl + "!/plugin.xml");
                StandardPluginLocation standardPluginLocation = new StandardPluginLocation(context, manifest);
                locList.add(standardPluginLocation);
            } catch (MalformedURLException e1) {
            }
        }
        
        try {
            PluginLocation[] locations = locList.toArray(new PluginLocation[locList.size()]);
            pluginManager.publishPlugins(locations);
            Collection<PluginDescriptor> allPluginDescriptors = pluginManager.getRegistry().getPluginDescriptors();
            pluginDescriptors = new ArrayList<PluginDescriptor>();
            for (PluginDescriptor pluginDescriptor : allPluginDescriptors) {
                String id = pluginDescriptor.getId();
                try {
                    pluginManager.activatePlugin(id);
                    pluginDescriptors.add(pluginDescriptor);
                } catch (Exception e) {
                    logger.log(Level.INFO, "Unable to activate plugin " + id);
                }
            }
        } catch (JpfException e) {
            throw new RuntimeException(e);
        }
    }

    private File[] searchExistingPlugins() {
        File pluginsDir = new File(appDataDir, Consts.PLUGINS_DIR);
        logger.info("Plugins dir: " + pluginsDir.getAbsolutePath());
        return pluginsDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase(Locale.ENGLISH).endsWith(".frp");
            }
        });
    }

    public boolean isOtherMatchingPlugin(String url, String otherThanThisUniqueId) {
        for (PluginDescriptor pluginDescriptor : pluginDescriptors) {
            String urlRegexAttribute = getAttribute("urlRegex", "xxxxxxxxxxxx", pluginDescriptor);
            Pattern pattern = Pattern.compile(urlRegexAttribute, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(url);
            if (matcher.find() && !pluginDescriptor.getUniqueId().equals(otherThanThisUniqueId)) {
            	return true;
            }
        }
        return false;
    }
    
    public PluginAndDescriptor getPluginForUrl(String url, Map<String, PremiumAccount> premAccountsMap) {
        for (PluginDescriptor pluginDescriptor : pluginDescriptors) {
            if (isPremium(pluginDescriptor) && !premAccountsMap.containsKey(pluginDescriptor.getId()) && isOtherMatchingPlugin(url, pluginDescriptor.getUniqueId())) {
            	logger.info("Going to search for no premium pluging for " + url);
                continue;
            }
            if (!isPremium(pluginDescriptor) && premAccountsMap.containsKey(pluginDescriptor.getId()) && isOtherMatchingPlugin(url, pluginDescriptor.getUniqueId())) {
            	logger.info("Going to search for premium pluging for " + url);
                continue;
            }
            String urlRegexAttribute = getAttribute("urlRegex", "xxxxxxxxxxxx", pluginDescriptor);
            Pattern pattern = Pattern.compile(urlRegexAttribute, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(url);
            if (matcher.find()) {
                try {
                    ShareDownloadService shareDownloadService = (ShareDownloadService) pluginManager.getPlugin(pluginDescriptor.getId());
                    PluginAndDescriptor pluginAndDescriptor = new PluginAndDescriptor();
                    pluginAndDescriptor.shareDownloadService = shareDownloadService;
                    pluginAndDescriptor.downloadUrl = url;
                    pluginAndDescriptor.pluginDescriptor = pluginDescriptor;
                    return pluginAndDescriptor;
                } catch (PluginLifecycleException e) {
                    throw new RuntimeException("Not found plugin for id " + pluginDescriptor.getId(), e);
                }
            }
        }
        return null;
    }
    
    public static boolean isPremium(PluginDescriptor pluginDescriptor) {
        return DescriptorUtils.getAttribute("premium", false, pluginDescriptor);
    }
    
    public static String getAttribute(final String name, final String defaultValue, final PluginDescriptor descriptor) {
        final PluginAttribute attribute = descriptor.getAttribute(name);
        if (attribute == null) {
            //     logger.warning(name + " attribute was not found in plugin manifest for plugin " + descriptor.getId());
            return defaultValue;
        } else return attribute.getValue();
    }
    
    public static class PluginAndDescriptor {
        public String downloadUrl;
        public ShareDownloadService shareDownloadService;
        public PluginDescriptor pluginDescriptor;
    }
}
