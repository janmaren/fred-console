package cz.stmare.freerapid.download;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import cz.stmare.freerapid.core.tasks.ConsoleDownloadTask;
import cz.stmare.freerapid.model.DownloadByConsoleFile;
import cz.stmare.freerapid.plugimpl.ConsoleStorageSupport;
import cz.stmare.freerapid.plugimpl.ConsoleSupportImpl;
import cz.stmare.freerapid.plugimpl.PluginSearcher;
import cz.stmare.freerapid.plugimpl.PluginSearcher.PluginAndDescriptor;
import cz.stmare.freerapid.plugins.webclient.ConsoleDownloadClient;
import cz.stmare.freerapid.plugins.webclient.hoster.PremiumAccount;
import cz.stmare.util.HomeManager;
import cz.vity.freerapid.plugimpl.StandardPluginContextImpl;
import cz.vity.freerapid.plugins.webclient.interfaces.ConfigurationStorageSupport;
import cz.vity.freerapid.plugins.webclient.interfaces.DialogSupport;
import cz.vity.freerapid.plugins.webclient.interfaces.HttpFile;
import cz.vity.freerapid.plugins.webclient.interfaces.MaintainQueueSupport;
import cz.vity.freerapid.plugins.webclient.interfaces.ShareDownloadService;

public class DownloadTest {
    @Test
    @Ignore
    public void downloadTest() {
        HomeManager homeManager = new HomeManager("VitySoft", "FRD");
        PluginSearcher pluginWrapper = PluginSearcher.getInstance(homeManager.getAppDataDirectory().toString());
        
        String downloadUrl = "https://fastshare.cz/5650655/lunetic-kouzelna-noc.mp3#.WoxxwOYo_mg";
        Map<String, PremiumAccount> premAccountsMap = new HashMap<>();
		PluginAndDescriptor pluginForUrl = pluginWrapper.getPluginForUrl(downloadUrl, premAccountsMap);
        ShareDownloadService plugin = pluginForUrl.shareDownloadService;
        if (plugin == null) {
            return;
        }
        URL fileUrl = null;
        try {
            fileUrl = new URL(downloadUrl);
        } catch (MalformedURLException e1) {
            return;
        }
        File saveToDirectory = new File(System.getProperty("user.home") + "/" + "temp");
        String description = "Nejaky popis";

        HttpFile httpFile = new DownloadByConsoleFile(fileUrl, saveToDirectory, description);

        ConsoleDownloadClient httpDownloadClient = new ConsoleDownloadClient();
//        httpDownloadClient.setProxyType(Proxy.Type.HTTP);
//        httpDownloadClient.setProxyURL("127.0.0.1");
//        httpDownloadClient.setProxyPort(8088);
        httpDownloadClient.initClient();
        
        ConsoleDownloadTask consoleDownloadTask = new ConsoleDownloadTask(httpFile, httpDownloadClient); 
        try {
            DialogSupport dialogSupport = new ConsoleSupportImpl(new File("c:\\temp\\dialog"), plugin.getName());
            ConfigurationStorageSupport configurationStorageSupport = new ConsoleStorageSupport();
            MaintainQueueSupport maintainQueueSupport = null;
            plugin.setPluginContext(StandardPluginContextImpl.create(dialogSupport, configurationStorageSupport, maintainQueueSupport));
            plugin.run(consoleDownloadTask);
            System.out.println(plugin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    @Test
    @Ignore
    public void downloadWebshareTest() {
        HomeManager homeManager = new HomeManager("VitySoft", "FRD");
        PluginSearcher pluginWrapper = PluginSearcher.getInstance(homeManager.getAppDataDirectory().toString());
        
        String downloadUrl = "https://webshare.cz/#/file/68l4n22r5a/hc-kometa-brno-hymna-official-anthem-mp3-mp3-mp3";
        Map<String, PremiumAccount> premAccountsMap = new HashMap<>();
		PluginAndDescriptor pluginForUrl = pluginWrapper.getPluginForUrl(downloadUrl, premAccountsMap);
        ShareDownloadService plugin = pluginForUrl.shareDownloadService;
        if (plugin == null) {
            return;
        }
        URL fileUrl = null;
        try {
            fileUrl = new URL(downloadUrl);
        } catch (MalformedURLException e1) {
            return;
        }
        File saveToDirectory = new File(System.getProperty("user.home") + "/" + "temp");
        String description = "Nejaky popis";

        HttpFile httpFile = new DownloadByConsoleFile(fileUrl, saveToDirectory, description);

        ConsoleDownloadClient httpDownloadClient = new ConsoleDownloadClient();
//        httpDownloadClient.setProxyType(Proxy.Type.HTTP);
//        httpDownloadClient.setProxyURL("127.0.0.1");
//        httpDownloadClient.setProxyPort(8088);
        httpDownloadClient.initClient();
        
        ConsoleDownloadTask consoleDownloadTask = new ConsoleDownloadTask(httpFile, httpDownloadClient); 
        try {
            DialogSupport dialogSupport = new ConsoleSupportImpl(new File("c:\\temp\\dialog"), plugin.getName());
            ConfigurationStorageSupport configurationStorageSupport = new ConsoleStorageSupport();
            MaintainQueueSupport maintainQueueSupport = null;
            plugin.setPluginContext(StandardPluginContextImpl.create(dialogSupport, configurationStorageSupport, maintainQueueSupport));
            plugin.run(consoleDownloadTask);
            System.out.println(plugin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }    
    
    @Test
    @Ignore
    public void downloadDatoidTest() {
        HomeManager homeManager = new HomeManager("VitySoft", "FRD");
        PluginSearcher pluginWrapper = PluginSearcher.getInstance(homeManager.getAppDataDirectory().toString());
        
        String downloadUrl = "https://datoid.cz/m66MaF/nomore-mp3";
        Map<String, PremiumAccount> premAccountsMap = new HashMap<>();
		PluginAndDescriptor pluginForUrl = pluginWrapper.getPluginForUrl(downloadUrl, premAccountsMap);
        ShareDownloadService plugin = pluginForUrl.shareDownloadService;
        if (plugin == null) {
            return;
        }
        URL fileUrl = null;
        try {
            fileUrl = new URL(downloadUrl);
        } catch (MalformedURLException e1) {
            return;
        }
        File saveToDirectory = new File(System.getProperty("user.home") + "/" + "temp");
        String description = "Nejaky popis";

        HttpFile httpFile = new DownloadByConsoleFile(fileUrl, saveToDirectory, description);

        ConsoleDownloadClient httpDownloadClient = new ConsoleDownloadClient();
//        httpDownloadClient.setProxyType(Proxy.Type.HTTP);
//        httpDownloadClient.setProxyURL("127.0.0.1");
//        httpDownloadClient.setProxyPort(8088);
        httpDownloadClient.initClient();
        
        ConsoleDownloadTask consoleDownloadTask = new ConsoleDownloadTask(httpFile, httpDownloadClient); 
        try {
            DialogSupport dialogSupport = new ConsoleSupportImpl(new File("c:\\temp\\dialog"), plugin.getName());
            ConfigurationStorageSupport configurationStorageSupport = new ConsoleStorageSupport();
            MaintainQueueSupport maintainQueueSupport = null;
            plugin.setPluginContext(StandardPluginContextImpl.create(dialogSupport, configurationStorageSupport, maintainQueueSupport));
            plugin.run(consoleDownloadTask);
            System.out.println(plugin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }        
}
